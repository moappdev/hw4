package com.example.healthyMe;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.researchstack.backbone.answerformat.AnswerFormat;
import org.researchstack.backbone.answerformat.BooleanAnswerFormat;
import org.researchstack.backbone.answerformat.ChoiceAnswerFormat;
import org.researchstack.backbone.answerformat.IntegerAnswerFormat;
import org.researchstack.backbone.model.Choice;
import org.researchstack.backbone.result.StepResult;
import org.researchstack.backbone.result.TaskResult;
import org.researchstack.backbone.step.InstructionStep;
import org.researchstack.backbone.step.QuestionStep;
import org.researchstack.backbone.step.Step;
import org.researchstack.backbone.task.OrderedTask;
import org.researchstack.backbone.ui.ViewTaskActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.CONNECTIVITY_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SurveyFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class SurveyFragment extends Fragment {
    User current_user;
    SharedPreferences sharedpref;
    HashMap<String, ArrayList<Question>> section_map = new HashMap<String, ArrayList<Question>>();
    ArrayList<Question> list_all_questions = new ArrayList<Question>();
    ArrayList<String> list_all_answers;
    private static final int SURVEY_QUESTION1  = 1;
    private static final int SURVEY_QUESTION2  = 2;
    private static final int SURVEY_FROM_DIET  = 3;
    private static final int SUMMARY_SURVEY  = 4;
    public static final  String INSTRUCTION = "identifier";
    public static final  String SAMPLE_SURVEY  = "sample_survey";
    private AppCompatButton surveyButton;
    NetworkCallResponse api_response;


    private OnFragmentInteractionListener mListener;

    public SurveyFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_survey, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    private void getCurrentUserSessionDetailsFronSharedPreferences() {
        sharedpref = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
        String current_user_json = sharedpref.getString("CurrentUserSession", "");
        Gson gson = new Gson();
        if(current_user_json!=null){ current_user = gson.fromJson(current_user_json,User.class);}
    }

    private boolean isConnectedtoInternet() {
        ConnectivityManager cm= (ConnectivityManager) getActivity().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo ni= cm.getActiveNetworkInfo();
        if(ni!=null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
        findViewByIds();
        getCurrentUserSessionDetailsFronSharedPreferences();
        if(isConnectedtoInternet()) {
            getAllTheQuestionsFromApi();
        }
        else{
            displayToast("No Internet Connection. Please ensure you have a valid network connection to proceed further...");
        }
        surveyButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                launchSurvey();
            }
        });
    }

    private void getResponsesForCurrentUser() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getURLToRetrieveUserResponses())
                .get()
                .addHeader("token", current_user.usertoken.toString())
                .addHeader("username", current_user.username.toString())
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    try {
                        api_response = JsonParser.ParseGenericResponseObject.doJsonParsing(response.body().string());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (api_response != null){
                                if(!api_response.getSuccess()) {
                                    launchSurvey();
                                } else {
                                    displayToast("You have already taken the survey. Come back for the new ones.");
                                }
                            }
                            else {
                                displayToast("Could not save results. Please try later");
                            }
                        }
                    });

                }
            }
        });
    }

    private String getURLToRetrieveUserResponses() {
        return getResources().getString(R.string.API_URL)+"getSurveyResponses";
    }

    private void getAllTheQuestionsFromApi() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(getURLToRetrieveSurveyQuestions())
                .get()
                .addHeader("token", current_user.usertoken.toString())
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                }
                else{
                    try {
                        list_all_questions = JsonParser.ParseQuestionsList.doJsonParsing(response.body().string());
                        segregateQuestions();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void segregateQuestions() {
        for(int i=0; i<list_all_questions.size(); i++){
            if(!section_map.containsKey(list_all_questions.get(i).section.toString())){
                ArrayList<Question> newlist = new ArrayList<Question>();
                newlist.add(list_all_questions.get(i));
                section_map.put(list_all_questions.get(i).section.toString(),newlist);
            }
            else{
                ArrayList<Question> existing_list = section_map.get(list_all_questions.get(i).section.toString());
                existing_list.add(list_all_questions.get(i));
                section_map.put(list_all_questions.get(i).section.toString(),existing_list);
            }
        }
    }


    private String getURLToRetrieveSurveyQuestions() {
        return getResources().getString(R.string.API_URL)+"showQuestions";
    }

    private void findViewByIds() {
        surveyButton = (AppCompatButton) getActivity().findViewById(R.id.survey_button);
    }

    private void launchSurvey()
    {
        InstructionStep instructionStep = new InstructionStep(INSTRUCTION,
                "Survey",
                "Welcome! We need to collect just a little health information from you before we begin. Circle the correct answer");
        instructionStep.setStepTitle(R.string.survey);

        QuestionStep has_highbloodpressure_step = new QuestionStep("has_highbloodpressure_step");
        has_highbloodpressure_step.setStepTitle(R.string.survey);
        has_highbloodpressure_step.setTitle("Has your doctor told you that you have high blood pressure?");
        has_highbloodpressure_step.setAnswerFormat(new BooleanAnswerFormat(getString(R.string.rsb_yes),
                getString(R.string.rsb_no)));
        has_highbloodpressure_step.setOptional(false);
        OrderedTask task = new OrderedTask(SAMPLE_SURVEY, instructionStep, has_highbloodpressure_step);
        Intent intent = ViewTaskActivity.newIntent(getActivity(), task);
        getActivity().startActivityForResult(intent, SURVEY_QUESTION1);
    }


    private void launchSurveyQuestion2() {
        QuestionStep has_highbloodpressure_step = new QuestionStep("has_prescribed_pills");
        has_highbloodpressure_step.setStepTitle(R.string.survey);
        has_highbloodpressure_step.setTitle("Has your doctor prescribed medication or pills to treat your high blood pressure?");
        has_highbloodpressure_step.setAnswerFormat(new BooleanAnswerFormat(getString(R.string.rsb_yes),
                getString(R.string.rsb_no)));
        has_highbloodpressure_step.setOptional(false);
        OrderedTask task = new OrderedTask(SAMPLE_SURVEY, has_highbloodpressure_step);
        Intent intent = ViewTaskActivity.newIntent(getActivity(), task);
        getActivity().startActivityForResult(intent, SURVEY_QUESTION2);
    }

    private void launchSummaryStep() {
        InstructionStep summaryStep = new InstructionStep("survey_summary_step",
                "Right. Off you go!",
                "Thank you for your time. Your participation is very important to us.");
        OrderedTask task = new OrderedTask(SAMPLE_SURVEY, summaryStep);
        Intent intent = ViewTaskActivity.newIntent(getActivity(), task);
        getActivity().startActivityForResult(intent, SUMMARY_SURVEY);
    }

    @NonNull
    private void launchAllSectionsWithout(String section_to_delete)
    {
        List<String> my_order_of_sections = Arrays.asList("medication usage","diet","physical activity","smoking","weight management","alchohol", "alchoholblank");
        HashMap<String, ArrayList<Question>> start_from_section_map = new HashMap<String, ArrayList<Question>>(section_map);
        if(start_from_section_map.containsKey(section_to_delete)) {
            start_from_section_map.remove(section_to_delete);
            my_order_of_sections = Arrays.asList("diet","physical activity","smoking","weight management","alchohol","alchoholblank");
        }
        List<Step> steps = new ArrayList<>();
        for (String section_name : my_order_of_sections) {
            if(section_name.equalsIgnoreCase("medication usage")){
                InstructionStep medicationStep = new InstructionStep(section_name+"instruction",
                        "Medication Usage",
                        "The following questions ask about your health behavior activities during the past 7 days. For each question, circle the number of days that you performed that activity.");
                medicationStep.setStepTitle(R.string.survey);
                steps.add(medicationStep);
            };
            if(section_name.equalsIgnoreCase("weight management")){
                InstructionStep wightmngts_step = new InstructionStep(section_name+"instuction",
                        "Weight Management",
                        "The following questions ask about your efforts to manage your weight during the last 30 days. If you were sick during the past month, please think back to the previous month that you were not sick. Circle the one answer that best describes what you do to lose weight or maintain your weight.");
                wightmngts_step.setStepTitle(R.string.survey);
                steps.add(wightmngts_step);
            };
            if(section_name.equalsIgnoreCase("alchohol")){
                InstructionStep alcohol_step = new InstructionStep(section_name+"instuction",
                        "Alcohol Consumption",
                        "The next three questions are about alcohol consumption. A drink of alcohol is defined as: \n One, 12 oz. can or bottle of beer; \n One, 4 ounce glass of wine; \n One, 12 oz. can or bottle of wine cooler; \n One mixed drink or cocktail; \n Or 1 shot of hard liquor.");
                alcohol_step.setStepTitle(R.string.survey);
                steps.add(alcohol_step);
            };
            if(section_name.equals("medication usage")){
                for(Question present_question : start_from_section_map.get(section_name) ){
                    AnswerFormat questionFormat = new ChoiceAnswerFormat(AnswerFormat.ChoiceAnswerStyle
                            .SingleChoice,
                            new Choice<>("0", "0"),
                            new Choice<>("1", "1"),
                            new Choice<>("2", "2"),
                            new Choice<>("3", "3"),
                            new Choice<>("4", "4"),
                            new Choice<>("5", "5"),
                            new Choice<>("6", "6"),
                            new Choice<>("7", "7"),
                            new Choice<>("I have not been prescribed blood pressure pills.", "0") );

                    QuestionStep questionStep = new QuestionStep(present_question.getId().toString(), present_question.title.toString(), questionFormat);
                    questionStep.setPlaceholder(present_question.section.toString());
                    questionStep .setStepTitle(R.string.back);
                    questionStep.setOptional(false);
                    steps.add(questionStep);
                }
            }
            if(!section_name.equals("weight management")  && !section_name.equals("alchoholblank") && !section_name.equals("medication usage")){
                for(Question present_question : start_from_section_map.get(section_name) ){
                    AnswerFormat questionFormat = new ChoiceAnswerFormat(AnswerFormat.ChoiceAnswerStyle
                            .SingleChoice,
                            new Choice<>("0", "0"),
                            new Choice<>("1", "1"),
                            new Choice<>("2", "2"),
                            new Choice<>("3", "3"),
                            new Choice<>("4", "4"),
                            new Choice<>("5", "5"),
                            new Choice<>("6", "6"),
                            new Choice<>("7", "7"));

                    QuestionStep questionStep = new QuestionStep(present_question.getId().toString(), present_question.title.toString(), questionFormat);
                    questionStep.setPlaceholder(present_question.section.toString());
                    questionStep .setStepTitle(R.string.back);
                    questionStep.setOptional(false);
                    steps.add(questionStep);
                }
            }
            else if(section_name.equals("weight management")){
                for(Question present_question : start_from_section_map.get(section_name) ){
                    AnswerFormat questionFormat = new ChoiceAnswerFormat(AnswerFormat.ChoiceAnswerStyle
                            .SingleChoice,
                            new Choice<>("Strongly Disagree", "1"),
                            new Choice<>("Disagree", "2"),
                            new Choice<>("Not Sure", "3"),
                            new Choice<>("Agree", "4"),
                            new Choice<>("Strongly Agree", "5"));
                    QuestionStep questionStep = new QuestionStep(present_question.getId().toString(), present_question.title.toString(), questionFormat);
                    questionStep.setPlaceholder(present_question.section.toString());
                    questionStep .setStepTitle(R.string.back);
                    questionStep.setOptional(false);
                    steps.add(questionStep);
                }
            }
            else if(section_name.equalsIgnoreCase("alchoholblank")){
                for(Question present_question : start_from_section_map.get(section_name) ){
                    QuestionStep questionStep = new QuestionStep(present_question.getId().toString(), present_question.title.toString(), new IntegerAnswerFormat(0, 20));
                    questionStep.setPlaceholder("write in #");
                    questionStep .setStepTitle(R.string.back);
                    questionStep.setOptional(false);
                    steps.add(questionStep);
                }
            }

        }

        OrderedTask task = new OrderedTask(SAMPLE_SURVEY, steps);
        Intent intent = ViewTaskActivity.newIntent(getActivity(), task);
        getActivity().startActivityForResult(intent, SURVEY_FROM_DIET);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            processSurveyResult((TaskResult) data.getSerializableExtra(ViewTaskActivity.EXTRA_TASK_RESULT), requestCode);
        }
    }

    private void processSurveyResult(TaskResult result, int requestcode)
    {
        if(requestcode == SURVEY_QUESTION1)
        {
            if(result.getResults().get("has_highbloodpressure_step").getResult().toString()=="true"){
                launchSurveyQuestion2();            }
            else{
                launchAllSectionsWithout("medication usage");
            }
        }
        else if(requestcode == SURVEY_QUESTION2)
        {
            if(result.getResults().get("has_prescribed_pills").getResult().toString()=="false"){
                launchAllSectionsWithout("medication usage");
            }
            else{
                launchAllSectionsWithout("Nothing");
            }
        }
        else if(requestcode == SUMMARY_SURVEY)
        {
            displayToast("Survey Completed Successfully!!");
        }
        else
        {
            calculateAndSaveResults(result);
            if(list_all_answers.size()>0){
                postAnswersToServer(list_all_answers);
            }
            else{
                displayToast("Something Went Wrong. Please try again.");
            }
        }

    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void calculateAndSaveResults(TaskResult result) {
        list_all_answers = new ArrayList<String>();
        for(String id : result.getResults().keySet())
        {
            StepResult stepResult = result.getStepResult(id);
            if(stepResult!=null) {
                list_all_answers.add(id+" "+stepResult.getResult().toString());
            }
        }
    }

    private void displayToast(String s) {
        Toast.makeText(getActivity(),s,Toast.LENGTH_LONG).show();
    }

    private void postAnswersToServer(ArrayList<String> list_of_answers) {
        Gson gson = new Gson();
        String[] stockArr = new String[list_of_answers.size()];
        stockArr = list_of_answers.toArray(stockArr);
        Log.d("Hell1",gson.toJson(stockArr));
        Log.d("Hell1",gson.toJson(list_of_answers));
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, gson.toJson(list_of_answers));
        Request request = new Request.Builder()
                .url(getURLToSubmitUserResponses())
                .post(body)
                .addHeader("username", current_user.username.toString())
                .addHeader("token", current_user.usertoken.toString())
                .addHeader("content-type", "application/json")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    try {
                        api_response = JsonParser.ParseGenericResponseObject.doJsonParsing(response.body().string());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (api_response != null){
                                if(api_response.getSuccess()) {
                                    displayToast("Answers submitted successfuly!!");
                                    launchSummaryStep();
                                } else {
                                    displayToast("Login Failed." + api_response.getStatus().toString());
                                }
                            }
                            else {
                                displayToast("Could not save resuls. Please try later");
                            }
                        }
                    });

                }
            }
        });
    }

    private String getURLToSubmitUserResponses() {
        return getResources().getString(R.string.API_URL)+"submitAnswers";
    }

}
