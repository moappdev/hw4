package com.example.healthyMe;

/**
 * Created by Rama Vamshi Krishna on 11/07/2017.
 */

public class Question {
    String id, title, section, answer;

    public Question(){

    }
    public Question(String id, String title, String section) {
        this.id = id;
        this.title = title;
        this.section = section;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
